# discover-spurious

PerturbVar
- Configure top-k inside file:
    - PerturbVar-BigVul.ipynb
    - PerturbVar-Devign.ipynb
  
PerturbAPI
- Generate appendix file first
    - PerturbAPI-GenerateAppendix-BigVul.ipynb
    - PerturbAPI-GenerateAppendix-Devign.ipynb
- Run
     - PerturbAPI-Big-Vul.ipynb
     - PerturbAPI-Devign.ipynb



PerturbJoint
- Run PerturbAPI steps, then PerturbVar atop
    
